ziproxy (3.3.2-7) unstable; urgency=medium

  * debian/control:
    - Bump Standards-Version to "4.6.2".
    - Replace lsb-base to sysvinit-utils in Depends.
  * debian/patches/01_ziproxyconf.diff: Add Forwarded.

 -- Marcos Talau <talau@debian.org>  Thu, 07 Sep 2023 01:28:01 +0530

ziproxy (3.3.2-6) unstable; urgency=medium

  [ Debian Janitor ]
  * debian/control: Drop versioned constraint on lsb-base (>= 3.0-10) in
    Depends.

  [ Marcos Talau ]
  * Build without SASL support (Closes: #1023918). Thanks to Bastian Germann
    for the report.
  * debian/copyright: Update packaging copyright years.

 -- Marcos Talau <talau@debian.org>  Thu, 05 Jan 2023 11:41:57 -0300

ziproxy (3.3.2-5) unstable; urgency=medium

  * Update Maintainer mail.
  * debian/lintian-overrides:
    - Update non-standard-file-perm.
    - Update package-contains-documentation-outside-usr-share-doc.
  * debian/source/lintian-overrides:
    - Update very-long-line-length-in-source-file.

 -- Marcos Talau <talau@debian.org>  Fri, 09 Sep 2022 18:30:11 -0300

ziproxy (3.3.2-4) unstable; urgency=medium

  * debian/control: Remove sentences related to JPEG-2000 in the Description.
  * debian/copyright:
    - Add Clint Adams, Mattia Rizzolo, and Michael Hudson-Doyle
      to `debian/*' copyright stanza.
    - Change Upstream-Name to Ziproxy.
  * debian/default: Remove useless comments.
  * debian/docs: Remove the file `JPEG2000.txt'.
  * debian/lintian-overrides: Add comments about the overrides.
  * debian/rules: Add a comment about the use of `chmod'.
  * debian/tests/*: Improve all tests.
  * debian/tests/ziproxy: Rename to validation.

 -- Marcos Talau <marcos@talau.info>  Sat, 11 Jun 2022 12:59:09 -0300

ziproxy (3.3.2-3) unstable; urgency=medium

  * debian/control: Bump Standards-Version to 4.6.1, no changes.
  * debian/copyright: Update packaging copyright years.
  * debian/source/lintian-overrides: New. Add override due long lines in the
    files src/cfgfile.[ch].
  * debian/ziproxy.default: Rename to default.
  * debian/ziproxy.init: Rename to init.
  * debian/ziproxy.install: Rename to install.
  * debian/ziproxy.service: Rename to service.

 -- Marcos Talau <marcos@talau.info>  Fri, 10 Jun 2022 16:21:23 -0300

ziproxy (3.3.2-2) unstable; urgency=medium

  * Upload to unstable.
  * debian/control:
    - Bump Standards-Version to 4.6.0.1.
    - Update Vcs-* fields.
  * debian/rules: Remove '--with autoreconf' because it is default since DH 10.
  * debian/salsa-ci.yml: Add to provide CI tests for Salsa.
  * debian/tests/control:
    - Add restriction isolation-container.
    - Add ziproxy to Depends.

 -- Marcos Talau <marcos@talau.info>  Fri, 10 Sep 2021 19:54:31 -0300

ziproxy (3.3.2-1) experimental; urgency=low

  * New upstream release.
  * Using new DH level format. Consequently:
    - debian/compat: Remove.
    - debian/control: Change from 'debhelper' to 'debhelper-compat' in
      Build-Depends field and bump level to 13.
  * debian/control:
    - Add Pre-Depends field.
    - Add Rules-Requires-Root field.
    - Bump Standards-Version to 4.5.1.
    - Change Maintainer mail.
    - Change priority from extra to optional.
    - Update Vcs fields to salsa.
  * debian/copyright: Update years.
  * debian/lintian-overrides: Add overrides to /usr/share/ziproxy/error/*
  * debian/patches/:
    - 02_small_spelling.diff: Remove, upstream are using it.
    - 03_giflib5.diff: Remove, upstream are using it.
    - 04_gcc10.diff: Remove, upstream fixed the compilation problem.
      Thanks to Michael Hudson-Doyle.
    - 02_ziproxylogtool_typos.diff: New, fix typos in ziproxylogtool.
    - 03_image_gccwarn.diff: New, fix gcc warning in image.c
  * debian/postinst: Change /var/run to /run.
  * debian/README.Debian: Add note about jasper support.
  * debian/tests/*: Create autopkgtest.
  * debian/upstream/metadata: Add upstream metadata information.
  * debian/watch:
    - Replace some regex's to @TAGS@.
    - Update to version 4.
  * debian/ziproxy.default: Add comments about systemd.
  * debian/ziproxy.service: New systemd service file.

 -- Marcos Talau <marcos@talau.info>  Fri, 04 Jun 2021 19:36:19 -0300

ziproxy (3.3.1-2.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix FTBFS with gcc 10. (Closes: 958012)

 -- Michael Hudson-Doyle <mwhudson@debian.org>  Tue, 06 Oct 2020 13:54:59 +1300

ziproxy (3.3.1-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/rules
    - Build without jasper support.
  * debian/control
    - Use HTTPS in Vcs-* fields.
    - Drop build-depends on jasper.  Closes: #818210
    - Correctly format extended description.  Closes: #809492

 -- Mattia Rizzolo <mattia@debian.org>  Mon, 27 Jun 2016 10:06:07 +0000

ziproxy (3.3.1-2) unstable; urgency=medium

  * Upload to unstable.

 -- Marcos Talau <talau@users.sourceforge.net>  Tue, 29 Dec 2015 13:20:31 -0200

ziproxy (3.3.1-1) experimental; urgency=low

  * New upstream release.
    - DH level updated to 9.
    - Switch from autoconf to autoreconf
  * debian/control
    - Added lsb-base in Depends.
    - More detailed Description.
    - Standards-Version updated to 3.9.6
    - Vcs-* fields to canonical URI.
  * debian/copyright
    - Updated to 1.0 Machine-readable format.
  * debian/dirs
    - Removed, as it is dispensable.
  * debian/docs
    - Added THANKS file from upstream.
  * debian/patches/:
    - 01_ziproxyconf.diff: Now Ziproxy binds only the local interface by
      default (LP: #1250952). Thanks to Shelby Cain.
    - Build using giflib5. Thanks to Matthias Klose (Closes: #803294).
    - New patch 02_small_spelling.diff
    - Removed 02_new_libpng.diff, upstream fixed it.
    - series: Fixed quilt-series-without-trailing-newline
  * debian/rules
    - Added Hardening options.
    - Removed DH override for ChangeLog
    - Removed unnecessary args in dh_fixperms
    - Switch from ./configure to dh_auto_configure
  * debian/watch
    - Improved URL regexp.
  * debian/ziproxy.init
    - Now uses LSB init-functions.

 -- Marcos Talau <talau@users.sourceforge.net>  Mon, 09 Nov 2015 13:06:15 -0200

ziproxy (3.2.0-2) unstable; urgency=low

  * debian/control
    - Standards-Version updated to 3.9.3
      - No changes need
    - Change Build-Depends libpng12-dev to libpng-dev (Closes: #662576)

 -- Marcos Talau <talau@users.sourceforge.net>  Wed, 21 Mar 2012 16:38:02 -0300

ziproxy (3.2.0-1) unstable; urgency=low

  * New upstream release
  * debian/patches/02_off_nameservers_opt.diff
    - Removed, upstream introduces ./configure parameter for it
  * debian/control
    - Standards-Version updated
  * debian/ziproxy.init
    - Remove PIDFILE if process not exists (Closes: #616160)
      - Thanks to Witold Baryluk
    - Small fixes
  * New patch for libpng 1.5.2 (Closes: #635699)
    - Thanks to Nobuhiro Iwamatsu

 -- Marcos Talau <talau@users.sourceforge.net>  Thu, 20 Oct 2011 11:45:11 -0200

ziproxy (3.1.3-1) unstable; urgency=low

  * New upstream release (LP: #569611)
  * debian/patches/02_ziproxy_3.1.1_speedup.diff
    - Removed, merged upstream.
  * debian/preinst
    - Removed, old command moved to postinst (Closes: #566589)
      - Thanks piuparts ;-)
  * debian/control
    - Standards-Version updated
  * Disabled Nameservers option (Closes: #591417) (LP: #539874)
    - Thanks to all in 591417@ and the upstream!
    - debian/README.Debian
      - Reason for it added.
    - New patch: debian/02_off_nameservers_opt.diff

 -- Marcos Talau <talau@users.sourceforge.net>  Mon, 16 Aug 2010 20:22:32 -0300

ziproxy (3.1.1-1) unstable; urgency=low

  * New upstream release (Closes: #587039) [CVE-2010-2350]
    - Thanks to Moritz Muehlenhoff
  * debian/patches/02_ziproxy_genhtml_stats-bashism.diff
    - Removed, merged upstream.
  * debian/control
    - Renamed Vcs* address
  * debian/patches/02_ziproxy_3.1.1_speedup.diff
    - New patch for fix CPU load problem
  * debian/ziproxy.init
    - Removed $local_fs from Required-{Start,Stop}

 -- Marcos Talau <talau@users.sourceforge.net>  Fri, 25 Jun 2010 21:29:00 -0300

ziproxy (3.1.0-1) unstable; urgency=low

  * New upstream release (LP: #569611) (Closes: #584933) [CVE-2010-1513]
  * Fixed bashisms in ziproxy_genhtml_stats.sh (Closes: #581153)
    - Thanks to Raphael Geissert.
  * Updated debian/copyright
  * debian/ziproxy.init:
    - Added Required-* $remote_fs
    - Now using pidfile option from daemon
  * User/group name now in debian/ziproxy.default
  * Removed patch for Russian man page
    - Upstream removed Russian man pages due to lack of maintainer
  * config.* is now updating with debhelper
  * Changed maintainer mail

 -- Marcos Talau <talau@users.sourceforge.net>  Fri, 04 Jun 2010 17:50:03 -0300

ziproxy (2.7.2-1.1) unstable; urgency=low

  * NMU
  * Add Depends on passwd and adduser, as used by
    preinst and postrm.  closes: #566589.

 -- Clint Adams <schizo@debian.org>  Sat, 30 Jan 2010 23:03:26 -0500

ziproxy (2.7.2-1) unstable; urgency=low

  * Run as a system user (Closes: #543471, #543494)
    - Thanks to Kandalintsev Alexandre.
  * Small fixes in init
  * New upstream release (Closes: #521051) [CVE-2009-0804]
  * Update to DebSrc3.0
  * Use of DEP-3 compliant headers
  * Updated debian/copyright
  * Small fixes in maintainer scripts

 -- Marcos Talau <marcostalau@gmail.com>  Mon, 07 Dec 2009 23:03:54 -0200

ziproxy (2.7.0-1) unstable; urgency=low

  * New upstream release
    - New files from upstream: replace_ct.list and deny.list
    - Updated ziproxyconf patch
    - Don't use manpage ziproxylogtool.ru.1, because it have problems
  * Upgrade for the Debian Policy 3.8.1

 -- Marcos Talau <marcostalau@gmail.com>  Mon, 04 May 2009 06:36:08 -0300

ziproxy (2.6.0-1) unstable; urgency=low

  * New upstream release

 -- Marcos Talau <marcostalau@gmail.com>  Wed, 24 Dec 2008 19:49:38 -0200

ziproxy (2.5.2-2) unstable; urgency=low

  * Created a debian patch to ziproxy.conf - quilt was used.
  * Fixed a warning in dpkg-shlibdeps
  * Added support for libjasper
  * Upgrade for the Debian Policy 3.8.0
  * Few changes at debian/rules
  * Fixed Lintian problems.
    - debhelper-script-needs-versioned-build-depends
    - build-depends-on-obsolete-package

 -- Marcos Talau <marcostalau@gmail.com>  Sun, 29 Jun 2008 17:41:51 -0300

ziproxy (2.5.2-1) unstable; urgency=low

  * Initial release. (Closes: #458605)

 -- Marcos Talau <marcostalau@gmail.com>  Wed, 7 May 2008 19:04:03 -0300
